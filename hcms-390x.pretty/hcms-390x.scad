//
// HCMS-390X model
//

mm = 25.4;
$fn = 32;

wid = 0.4*mm;
len = 0.7*mm;
hgt = 0.2*mm;
pin_len = 0.17*mm;


module body() {
     cube( [wid, len, hgt]);
}

module pins() {
     translate( [0.05*mm, 0.1*mm, -pin_len]) {
	  for( i=[0:5]) {
	       translate( [0, 0.1*mm*i, 0]) {
		    cylinder( d=0.01*mm, h=pin_len);
		    translate( [0.3*mm, 0, 0])
			 cylinder( d=0.01*mm, h=pin_len);
	       }
	  }
     }
}

module display() {
     difference() {
	  body();

	  translate( [0.127*mm, 0.046*mm, hgt-0.02*mm])
	       for( i=[0:3]) {
		    translate( [0, 4.45*i, 0])
		    cube( [0.146*mm, 0.083*mm, 2]);
	       }
     }
     pins();
}


display();


